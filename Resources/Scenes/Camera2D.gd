extends Camera2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export (float) var zoom_speed = 5

func _ready():
    # Called every time the node is added to the scene.
    # Initialization here
    reset_zoom()
    pass

func reset_zoom():
    zoom = Vector2(0.5, 0.5)

    if get_parent().attach_object:
        # get_parent().attach_object.get_node("Sprite")
        zoom = Vector2(0.75, 0.75)

    # Reset drag margin to a quarter of the zoom
    var dm = zoom.x * .25

    drag_margin_left = dm
    drag_margin_right = dm
    drag_margin_top = dm
    drag_margin_bottom = dm

    print("Reset Zoom")
    pass

func _process(delta):
    pass

func _on_Player_capsule_hit():
    # Change zoom level when player capsule changes (enter/leave ship)
    reset_zoom()

    pass # replace with function body
