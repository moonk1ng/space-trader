extends RigidBody2D

signal capsule_hit
signal capsule_jettison

var capsule_attached = false
onready var attach_object = get_node(".")

enum {STATE_IDLE, STATE_JETTISON}

export (float) var vertical_thrust = 1
export (float) var horizontal_thrust = 3.5
export (float) var rotate = 1.25

export (float) var accel_force = 50
export (float) var rotate_force = 10

export (float) var MAX_THRUST = 50
export (float) var MAX_ROTATION = 10

var velocity = Vector2(0, 0) setget ,velocity_get

func velocity_get():
    return velocity

var Up = Vector2(0, -1)
var Right = Vector2(1, 0)
var jettisoning = false

func _ready():
    pass

func handle_movement(delta):
    var thrust = Vector2(0, 0)

    var rot = 0

    $Sprite.play("idle")

    if Input.is_action_pressed("ui_up"):
        thrust.y = -horizontal_thrust
    if Input.is_action_pressed("ui_down"):
        thrust.y = horizontal_thrust
    if Input.is_action_pressed("ui_left"):
        $Sprite.play("left")
        rot -= rotate
    if Input.is_action_pressed("ui_right"):
        $Sprite.play("right")
        rot += rotate

    if Input.is_action_pressed("ui_strafe_left"):
        thrust.x = -horizontal_thrust
        $Sprite.play("left")

    if Input.is_action_pressed("ui_strafe_right"):
        thrust.x = horizontal_thrust
        $Sprite.play("right")

    var temp_thrustx = clamp(thrust.x * accel_force, -MAX_THRUST, MAX_THRUST)
    var temp_thrusty = clamp(thrust.y * accel_force, -MAX_THRUST, MAX_THRUST)

    velocity = Vector2(temp_thrustx, temp_thrusty).rotated(rotation)

    var rotated = rot * rotate_force * delta

    apply_impulse(Up, velocity * delta)

    set_angular_velocity(clamp(get_angular_velocity() + rotated, -MAX_ROTATION, MAX_ROTATION))
    pass

func jettison():
    print("Capsule jettisoned!")
    emit_signal("capsule_jettison")
    show()
    $CollisionShape2D.disabled = false
    capsule_attached = false
    attach_object = get_node(".") # reference to the body we're now attached to
    jettisoning = true
    
    # Fire that capsule!
    apply_impulse(Up, (Right * 50).rotated(rotation))
    pass

func _physics_process(delta):
    # Only handle movement if we're not attached to another body
    if !capsule_attached:
        handle_movement(delta)
    pass

func _process(delta):
    if capsule_attached:
        # The capsule is attached to another body, we'll use their position.
        position = attach_object.position

    if Input.is_action_just_pressed("ui_jettison") && capsule_attached:
        jettison()

    pass

func _on_Capsule_body_entered(body):
    if jettisoning:
        print("Capsule is jettisoning, ignoring body")
        return

    emit_signal("capsule_hit")
    hide()
    $CollisionShape2D.disabled = true
    capsule_attached = true
    attach_object = body # reference to the body we're now attached to
    
    print("Capsule is now attached to: ", body.get_name())
    pass # replace with function body


func _on_capsule_exited_collision():
    jettisoning = false
    pass # replace with function body
