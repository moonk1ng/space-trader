extends RigidBody2D

signal capsule_exited_collision

export (bool) var capsule_attached = false
var last_capsule = null

export (float) var vertical_thrust = 1
export (float) var horizontal_thrust = 3.5
export (float) var rotate = 1
export (float) var MAX_THRUST = 1000
export (float) var accel_force = 200.0
export (float) var rotate_force = 10

var Up = Vector2(0, -1)

var capsule_jettisoning = false

func _ready():
    pass

func _physics_process(delta):
    var shooting = Input.is_action_pressed("ui_shot")
    var velocity = Vector2(0, 0)

    var thrust = Vector2(0, 0)

    var rot = 0

    $Sprite.play("idle")

    if capsule_attached:
        if Input.is_action_pressed("ui_up"):
            thrust.y = -horizontal_thrust
        if Input.is_action_pressed("ui_down"):
            thrust.y = horizontal_thrust
        if Input.is_action_pressed("ui_left"):
            $Sprite.play("left")
            rot -= rotate
        if Input.is_action_pressed("ui_right"):
            $Sprite.play("right")
            rot += rotate

        if Input.is_action_pressed("ui_strafe_left"):
            thrust.x = -horizontal_thrust
            $Sprite.play("left")

        if Input.is_action_pressed("ui_strafe_right"):
            thrust.x = horizontal_thrust
            $Sprite.play("right")

    var temp_thrustx = clamp(thrust.x * accel_force, -MAX_THRUST, MAX_THRUST)
    var temp_thrusty = clamp(thrust.y * accel_force, -MAX_THRUST, MAX_THRUST)

    #print(temp_thrustx)

    velocity = Vector2(temp_thrustx, temp_thrusty).rotated(rotation)

    var rotated = rot * rotate_force * delta

    apply_impulse(Up, velocity * delta)

    set_angular_velocity(get_angular_velocity() + rotated)

    pass

func _on_Ship_body_entered(body):
    print("Ship collied with: ", body.get_name())
    pass # replace with function body


func _on_Player_capsule_hit():
    print("Ship collided with a capsule!")
    if !capsule_attached && !capsule_jettisoning:
        capsule_attached = true
    pass # replace with function body


func _on_Player_body_exited(body):
    capsule_jettisoning = false
    emit_signal("capsule_exited_collision")
    pass # replace with function body


func _on_capsule_jettison():
    print("Ship sees the capsule jettison!")
    capsule_attached = false
    capsule_jettisoning = true
    pass # replace with function body
