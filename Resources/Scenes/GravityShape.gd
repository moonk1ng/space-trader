extends CollisionShape2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
    # Called every time the node is added to the scene.
    # Initialization here
    var sprite = get_parent().get_parent().get_node("Body").get_node("Sprite")
    get_shape().radius = sprite.get_texture().get_width() * sprite.get_scale().x
    pass

#func _process(delta):
#    # Called every frame. Delta is time since last frame.
#    # Update game logic here.
#    pass
